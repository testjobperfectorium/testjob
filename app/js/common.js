const sandwich = document.getElementById("sandwich"),
      aside = document.getElementById("aside"),
      asideToggle = document.getElementById("aside-toggle"),
      submenu = document.querySelectorAll(".nav .is--submenu"),
      profile = document.getElementById("profile"),
      btnSearch = document.getElementById("search-btn"),
      headerRow = document.getElementById("header-row"),
      inputsDate = document.querySelectorAll(".form-date .input"),
      reset = document.getElementById("reset");

function sandwichHandler() {
    const root = document.getElementById("root");
    if ( root.classList.contains("header--active") ) {
        root.classList.remove("header--active");
    } else {
        root.classList.add("header--active");
    }
}

function toggleClass(el) {
    if ( el.parentElement.classList.contains("is--active") == false ) {
        el.parentElement.classList.add("is--active");
    } else {
        el.parentElement.classList.remove("is--active");
    }
}

function mask(event) {
    var matrix = "____-__-__",
        i = 0,
        def = matrix.replace(/\D/g, ""),
        val = this.value.replace(/\D/g, "");
    if (def.length >= val.length) val = def;
    this.value = matrix.replace(/./g, function(a) {
        return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
    });
};

function toggleMenu(element) {
    if ( element.classList.contains("is--open") == false ) {
        element.classList.add("is--open");
    } else {
        element.classList.remove("is--open");
    }
}

document.addEventListener("DOMContentLoaded", function(){

    sandwich.addEventListener("click", function(){
        sandwichHandler();
        if ( profile.parentElement.classList.contains("is--active") == true ) {
            toggleClass(profile);
        }
    });

    if ( aside != null ) {
        aside.addEventListener("click", function(){
            if ( asideToggle.classList.contains("aside--open") == true ) {
                asideToggle.classList.remove("aside--open");
                asideToggle.classList.add("aside--close");
            } else {
                asideToggle.classList.remove("aside--close");
                asideToggle.classList.add("aside--open");
            }
        });
    }

    profile.addEventListener("click", function(e){
        e.preventDefault();
        toggleClass(profile);
    });

    btnSearch.addEventListener("click", function(e){
        e.preventDefault();
        if ( window.innerWidth >414 ) {
            sandwich.click();
        } else {
            toggleClass(headerRow);

            if ( profile.parentElement.classList.contains("is--active") == true ) {
                toggleClass(profile);
            }
        }
    });

    if ( reset != null ) {
        reset.addEventListener("click", function(e){
            e.preventDefault();
            Array.prototype.slice.call(inputsDate).forEach(function(currentInput) {
                currentInput.value = "";
            });
        });
    }

    
    Array.prototype.slice.call(submenu).forEach(function(currentValue) {
        currentValue.addEventListener( 'click', function(e){e.preventDefault(); toggleMenu(this);} );
    });

    Array.prototype.slice.call(inputsDate).forEach(function(currentInput) {
        currentInput.addEventListener( 'input', mask);
        currentInput.addEventListener("click", mask);
        currentInput.addEventListener("blur", mask);
    });

});

