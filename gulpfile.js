let gulp = require('gulp');
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    csso = require('gulp-csso'),
    imagemin = require('gulp-imagemin');

gulp.task('img-minify', () =>
    gulp.src('app/images/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('app/img'))
);

gulp.task('libs-js', function() {
  return gulp.src('app/js/libs/*.js')
    .pipe(concat('libs.min.js'))
    .pipe(uglify({
      toplevel: true
    }))
    .pipe(gulp.dest('app/js/'));
});

// gulp.task('libs-css', function() {
//   return gulp.src('app/css/libs/*.css')
//     .pipe(concat('libs.min.css'))
//     .pipe(csso())
//     .pipe(gulp.dest('app/css/'));
// });

// gulp.task('common-js', function() {
//   return gulp.src([
//     'app/js/components/**/*.js',
//     'app/js/events/*.js',
//     ])
//     .pipe(concat('common.js'))
//     .pipe(gulp.dest('app/js/'));
// });

gulp.task('sass', function () {
  return gulp.src('app/sass/**/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: 'app'
        },
        notify: false
    });
});

gulp.task('autoPrefixer', () =>
    gulp.src('app/css/**/*.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('app/css'))
);

gulp.task('watch', ['browser-sync', 'sass', 'libs-js', 'autoPrefixer'], function () {
  gulp.watch('app/sass/**/*.scss', ['sass']);
  gulp.watch('app/*.html', browserSync.reload);
  gulp.watch([
    'app/css/**/*.css'
  ], ['autoPrefixer']).on('change', browserSync.reload);
  // gulp.watch([
  //   'app/js/components/**/*.js'
  // ], ['common-js']).on('change', browserSync.reload);
});